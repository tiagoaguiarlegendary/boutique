@extends('layouts/edit-form', [
    'createText' => trans('admin/components/general.create') ,
    'updateText' => trans('admin/components/general.update'),
    'helpTitle' => trans('admin/components/general.about_components_title'),
    'helpText' => trans('admin/components/general.about_components_text'),
    'formAction' => ($item) ? route('components.update', ['component' => $item->id]) : route('components.store'),

])

{{-- Page content --}}
@section('inputFields')

@include ('partials.forms.edit.name', ['translated_name' => trans('admin/components/table.title')])
@include ('partials.forms.edit.category-select', ['translated_name' => trans('general.category'), 'fieldname' => 'category_id','category_type' => 'component'])
@include ('partials.forms.edit.quantity')



@stop
